<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class FrontEnd
{

    public static function dateEnFrancais($created_at)
    {
        return Carbon::parse($created_at)->translatedFormat('d F Y');
    }

    public static function cutText($contenu, $limit)
    {
        if (strlen($contenu) < $limit)
        {
            $resultat = $contenu;
        }
            else
        {
            $resultat = strip_tags(substr($contenu, 0, strpos($contenu, ' ', $limit))) . ' ...';
        }

        return $resultat;
    }

    public static function wrapWord($str, int $lenth = 0, $remove_html = true, $pad=" ...")
    {
        $str = html_entity_decode($str);
        if ($remove_html){
            $str = strip_tags($str);
        }
        $str = preg_replace(array('/\s{2,}/','/[\t\n]/'),' ', $str);
        if (strlen($str) <= $lenth) { return $str; }
        $newstring = substr($str, 0, strrpos(substr($str, 0, $lenth),' '));
        return $newstring.$pad;
    }

    public static function paginateArray($items, $perPage = 1, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), count($items), $perPage, $page, $options);
    }

    public static function tempsLecture($content)
    {
        $word_count = str_word_count(strip_tags($content));

        $minutes = floor($word_count / 250);
        $seconds = floor($word_count % 250 / (250 / 60));

        $str_minutes = ($minutes == 1) ? "minute" : "minutes";
        $str_seconds = ($seconds == 1) ? "seconde" : "secondes";

        if ($minutes == 0) {
            return "{$seconds} {$str_seconds}";
        }
        else {
            return $seconds > 0 ? "{$minutes} {$str_minutes} et {$seconds} {$str_seconds}" : "{$minutes} {$str_minutes}";
        }
    }

    public static function cutTextAndCloseTags($text, $limit) {

        $html = self::cutText($text, $limit);

        preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];

        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);
        if (count($closedtags) == $len_opened) {
            return $html;
        }
        $openedtags = array_reverse($openedtags);
        for ($i=0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)){
                $html .= '</'.$openedtags[$i].'>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Auteur extends Model
{

    public function articles()
    {
        return $this->belongsToMany(Article::class, PivotArticlesCategory::class);
    }

}

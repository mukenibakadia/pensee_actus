<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;


class Article extends Model
{

    use Resizable;

    public function auteurs()
    {
        return $this->belongsToMany(Auteur::class, PivotArticlesAuteur::class);
    }

    public function categories()
    {
        return $this->belongsToMany(ArticlesCategory::class, PivotArticlesCategory::class, 'article_id', 'articles_category_id');
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopeInfront($query)
    {
        return $query->where('is_in_front', 1);
    }

    public function scopeFlasNews($query)
    {
        return $query->where('flash_news', 1)->orderBy('created_at', 'desc');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

}

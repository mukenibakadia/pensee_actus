<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ArticlesCategory extends Model
{

    public function articles()
    {
        return $this->belongsToMany(Article::class, PivotArticlesCategory::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

}

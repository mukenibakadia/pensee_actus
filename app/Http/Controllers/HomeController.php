<?php

namespace App\Http\Controllers;

use App\Models\Info;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('actus.index');
    }

    public function contact()
    {
        $info = Info::where('id', 1)->first();
        return view('actus.contact')->with(['info' => $info]);
    }

}

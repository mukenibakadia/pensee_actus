<?php

namespace App\Http\Controllers;

use App\Models\ArticlesCategory;
use App\Support\Collection;
use Illuminate\Http\Request;

class ArticlesCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\ArticlesCategory  $articlesCategory
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $articles = (new Collection(ArticlesCategory::where('slug', $slug)->first()->articles))->paginate(5);
        $categoryName = ArticlesCategory::where('slug', $slug)->first()->nom;
        return view('actus.articles-by-category')->with(
            [
                'articles' => $articles,
                'categoryName' => $categoryName
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\ArticlesCategory  $articlesCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticlesCategory $articlesCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\ArticlesCategory  $articlesCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticlesCategory $articlesCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\ArticlesCategory  $articlesCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticlesCategory $articlesCategory)
    {
        //
    }

    public static function categories()
    {
        $categories = ArticlesCategory::all()->filter(function($category){
            return $category->articles->count() > 0;
        });

        return $categories;
    }
}

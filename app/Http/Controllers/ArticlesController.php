<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($category_slug, $article_slug)
    {
        $article = Article::where('slug', $article_slug)->first();
        if($article->clicks == null)
        {
            $article->clicks = 1;
            $article->save();
        }else{
            $article->clicks += 1;
            $article->save();
        }
        return view('actus.article-show')->with(['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }

    public static function mostRead($limit, $category_id = null)
    {
        $articles = Article::Published()->orderBy('clicks', 'desc')->get();

        if($category_id != null)
        {
            $articles = $articles->filter(function ($item) use ($category_id){
                if($item->categories->first()->id == $category_id)
                {
                    return $item;
                }
            });
        }else{
            $articles = $articles->take($limit);
        }

        return $articles;
    }
}

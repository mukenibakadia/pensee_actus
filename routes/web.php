<?php

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'actus.'], function () {

    Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');

    //articles
    Route::prefix('articles')->group(function () {
        Route::get('{slug}', 'App\Http\Controllers\ArticlesCategoriesController@show')->name('articles.by.category');
        Route::get('/{category}/{slug}', 'App\Http\Controllers\ArticlesController@show')->name('article.unique');
    });

    Route::get('/contactez-nous', 'App\Http\Controllers\HomeController@contact')->name('contact-us');


});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

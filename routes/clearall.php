<?php

Route::get('/clear-all', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');

    $rootURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
                === 'on' ? "https" : "http") . "://" .
          $_SERVER['HTTP_HOST'];

    $previousURL = url()->previous();

    if($rootURL == $previousURL)
    {
        $chemin = redirect()->back();

    } else {

        $segment = explode('/', $previousURL);

        if ($segment[3] != 'dashboard')
        {
            $chemin = redirect()->back();
        } else {
            $chemin = redirect()->route('voyager.dashboard');
        }

    }

    return $chemin->with([
        'message'    => 'Cache, Route, Config and Views are cleared',
        'alert-type' => 'success',
    ]);

});

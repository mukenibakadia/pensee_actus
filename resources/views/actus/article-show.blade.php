@extends('actus.layouts.master')

@section('meta')
    <title>{{ setting('site.title') }} | {{ __('Article') }} - {{ $article->titre }}</title>

    <meta property="og:url"           content="{{ url()->current() }}" />
    <meta property="og:type"          content="{{ setting('site.title') }}" />
    <meta property="og:title"         content="{{ $article->titre }}" />
    <meta property="og:description"   content="{!! FrontEnd::cutTextAndCloseTags($article->contenu, 800) !!}" />
    <meta property="og:image"         content="{{ Voyager::image($article->thumbnail('medium')) }}" />

    <meta name="twitter:card" content="{!! FrontEnd::cutTextAndCloseTags($article->contenu, 800) !!}" />
    <meta name="twitter:site" content="{{ setting('site.title') }}" />
    <meta name="twitter:creator" content="JTechnology" />
@endsection

@section('body')

    @include('actus.layouts.partials.body.body-article-show')

@endsection

@section('javascript')
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60651d774e9f848d"></script>
@endsection

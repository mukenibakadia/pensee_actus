<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">

    <head>
        @include('actus.layouts.partials.meta.meta')
        @include('actus.layouts.partials.meta.styles')
        @yield('css')
    </head>

    <body>

        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="preloader-circle"></div>
                    <div class="preloader-img pere-text">
                        <img src="{{ asset('actus/assets/img/logo/logo.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>

        <main>

            @include('actus.layouts.partials.header.header')

            @yield('body')

            @include('actus.layouts.partials.footer.footer')

        </main>

        @include('actus.layouts.partials.meta.scripts')
        @yield('javascript')

    </body>
</html>

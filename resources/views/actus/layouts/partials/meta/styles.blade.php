<link rel="stylesheet" href="{{  asset('actus/assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/ticker-style.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/flaticon.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/slicknav.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/animate.min.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/fontawesome-all.min.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/themify-icons.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/slick.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/nice-select.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/style.css') }}">
<link rel="stylesheet" href="{{  asset('actus/assets/css/responsive.css') }}">

<!--
Google Font
=========================== -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap">

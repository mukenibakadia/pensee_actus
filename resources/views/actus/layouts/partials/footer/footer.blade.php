<footer>
    <!-- Footer Start-->
    <div class="footer-area footer-padding fix">
         <div class="container">
             <div class="row d-flex justify-content-between">
                 <div class="col-xl-5 col-lg-5 col-md-7 col-sm-12">
                     <div class="single-footer-caption">
                         <div class="single-footer-caption">
                             <!-- logo -->
                             <div class="footer-logo">
                                 <a href="index.html"><img src="{{  asset('actus/assets/img/logo/logo.png') }}" alt=""></a>
                             </div>
                             <div class="footer-tittle">
                                 <div class="footer-pera">
                                     <p>Impartialinfo.net est un site congolais d’information sur le continent africain en générale et la RDC en particulier, créé et animé par des journalistes attachés à l’éthique et la déontologie. Notre devoir : vous décrire l’actualité congolaise et africaine avec professionnalisme.</p> {{-- Notre devise : les faits sont sacrés. --}}
                                 </div>
                             </div>
                             <!-- social -->
                             <div class="footer-social" style="color: #a8a8a8;">
                                 Suivez nous sur : &nbsp;
                                 <a href="#"><i class="fab fa-twitter"></i></a>
                                 <a href="#"><i class="fab fa-facebook"></i></a>
                                 <a href="#"><i class="fab fa-instagram"></i></a>
                                 {{-- <a href="#"><i class="fab fa-pinterest-p"></i></a> --}}
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6">
                     <div class="single-footer-caption mb-50 mt-60 footer-area">
                         <div class="footer-tittle">
                             <h4>Nos contacts</h4>
                         </div>
                         <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <p class="mb-0">+243 825 757 570</p>
                                <p class="mb-0">+243 894 707 943</p>
                                <p class="mb-0">Joignable 24/24</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <p class="mb-0">contact@impartialinfo.net</p>
                                <p class="mb-0">Envoyez nous un mail!</p>
                            </div>
                        </div>
                         {{-- <div class="instagram-gellay">
                             <ul class="insta-feed">
                                 <li><a href="#"><img src="{{ asset('actus/assets/img/post/instra1.jpg') }}" alt=""></a></li>
                                 <li><a href="#"><img src="{{ asset('actus/assets/img/post/instra2.jpg') }}" alt=""></a></li>
                                 <li><a href="#"><img src="{{ asset('actus/assets/img/post/instra3.jpg') }}" alt=""></a></li>
                                 <li><a href="#"><img src="{{ asset('actus/assets/img/post/instra4.jpg') }}" alt=""></a></li>
                                 <li><a href="#"><img src="{{ asset('actus/assets/img/post/instra5.jpg') }}" alt=""></a></li>
                                 <li><a href="#"><img src="{{ asset('actus/assets/img/post/instra6.jpg') }}" alt=""></a></li>
                             </ul>
                         </div> --}}
                     </div>
                 </div>
                 <div class="col-xl-3 col-lg-3 col-md-4  col-sm-6">
                    <div class="single-footer-caption mt-60">
                        <div class="footer-tittle">
                            <h4>Newsletter</h4>
                            <p>Inscrivez dans notre newsletter</p>
                            <!-- Form -->
                            <div class="footer-form" >
                                <div id="mc_embed_signup">
                                    <form action=""
                                       method="get" class="subscribe_form relative mail_part">
                                        <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                                        class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = ' Email Address '">
                                        <div class="form-icon">
                                           <button type="submit" name="submit" id="newsletter-submit" class="email_icon newsletter-submit button-contactForm">
                                               <i class="fa fa-envelope-square"></i>
                                           </button>
                                        </div>
                                        <div class="mt-10 info"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
     </div>
    <!-- footer-bottom aera -->
    <div class="footer-bottom-area">
        <div class="container">
            <div class="footer-border">
                <div class="row d-flex align-items-center justify-content-between">
                    <div class="col-lg-6">
                        <div class="footer-copy-right">
                            <p>
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tous droits reservés | <a href="{{ route('actus.home') }}">impartialinfo.net</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="footer-menu f-left">
                            <ul>
                                <li><a href="#">Conditions d'utilisations</a></li>
                                <li><a href="{{ route('actus.contact-us') }}">Contact</a></li>
                                <li style="color: #777777;font-size: 12px;text-transform: uppercase;">Developpé par <a href="#">JTechnology</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->
</footer>

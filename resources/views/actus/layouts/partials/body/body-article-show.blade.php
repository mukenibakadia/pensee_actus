<div class="container">
    <div class="row">
        <div class="col-12 my-4">
            @include('actus.layouts.breadcrumb')
        </div>
    </div>
</div>

<div class="about-area">
    <div class="container">
        <div class="row">

            <div class="col-lg-8">
                <!-- Trending Tittle -->
                <div class="about-right mb-90">
                    <div class="about-img mb-2">
                        @if (Storage::disk('public')->exists($article->image))
                            <img src="{{ Voyager::image($article->image) }}" alt="{{ $article->titre }}" style="width: 100%; height: 400px; object-fit: cover;">
                        @else
                            <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}" style="width: 100%; height: 400px; object-fit: cover;">
                        @endif
                    </div>

                    <small class="d-inline text-muted">
                        <i class="fa fa-calendar-check"></i> Publié le {{ FrontEnd::dateEnFrancais($article->created_at) }}
                        &nbsp;|&nbsp;
                        <i class="fa fa-user"></i> Par {{ $article->auteurs->first()->nom }}
                        &nbsp;|&nbsp;
                        <i class="fa fa-eye"></i> {{ $article->clicks ?? 0 }} @if($article->clicks != null && $article->clicks > 0) vues @else vue @endif
                        &nbsp;|&nbsp;
                        <i class="fa fa-list-alt"></i> Temps de lecture {{ FrontEnd::tempsLecture($article->contenu) }}
                    </small>

                    <br><br>

                    {!! $article->contenu !!}

                    <div class="social-share pt-30">
                        <div class="section-tittle">
                            <h3 class="mr-20">Partager cet article : </h3>
                            {{-- <ul>
                                <li><a href="#"><img src="assets/img/news/icon-ins.png" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/news/icon-fb.png" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/news/icon-tw.png" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/news/icon-yo.png" alt=""></a></li>
                            </ul> --}}

                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox_7npb"></div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">

                <div class="blog_right_sidebar">
                    @include('actus.layouts.partials.body.global.aside-most-read', ['category_id' => $article->categories->first()->id])

                    @include('actus.layouts.partials.body.global.aside-newsletter')

                </div>

            </div>

        </div>
    </div>
</div>


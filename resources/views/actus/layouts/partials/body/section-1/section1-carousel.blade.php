<div class="row">

    <div class="col-lg-8">
        @include('actus.layouts.partials.body.section-1.others.carousel-left')
    </div>

    <div class="col-lg-4">
        @include('actus.layouts.partials.body.section-1.others.carousel-right-recentes')
    </div>

</div>

<div class="blog_left_sidebar">
    <div class="row">
        <div class="blog_left_sidebar w-100">

            @if ($articles->count() > 0)
                @foreach ($articles as $article)
                    <article class="blog_item">
                        <div class="blog_item_img">
                            @if (Storage::disk('public')->exists($article->image))
                                <img class="card-img rounded-0" src="{{ Voyager::image($article->image) }}" alt="{{ $article->titre }}" style="height: 350px;object-fit: cover;width: 100%;">
                            @else
                                <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}" style="height: 350px;object-fit: cover;width: 100%;">
                            @endif
                        </div>

                        <div class="blog_details">
                            <a class="d-inline-block" href="{{ route('actus.article.unique', ['category' => $article->categories->first()->slug, 'slug' => $article->slug]) }}">
                                <h2>{{ $article->titre }}</h2>
                            </a>
                            <p class="mb-3">
                                {{ FrontEnd::wrapWord($article->contenu, 180) }} <a href="{{ route('actus.article.unique', ['category' => $article->categories->first()->slug, 'slug' => $article->slug]) }}">Lire la suite</a>
                            </p>
                            <ul class="blog-info-link">
                                <li class="text-muted"><i class="fa fa-calendar-check"></i> Publié le {{ FrontEnd::dateEnFrancais($article->created_at) }}</li>
                                <li class="text-muted"><i class="fa fa-user"></i> Par {{ $article->auteurs->first()->nom }}</li>
                                <li class="text-muted"><i class="fa fa-eye"></i> {{ $article->clicks ?? 0 }} @if($article->clicks != null && $article->clicks > 0) vues @else vue @endif</li>
                                <li class="text-muted"><i class="fa fa-list-alt"></i> {{ FrontEnd::tempsLecture($article->contenu) }} de lecture</li>
                            </ul>
                        </div>
                    </article>
                @endforeach

                {{-- <nav class="blog-pagination justify-content-center d-flex mt-0 mb-5">
                    <ul class="pagination">
                        <li class="page-item">
                            <a href="#" class="page-link" aria-label="Previous">
                                <i class="ti-angle-left"></i>
                            </a>
                        </li>
                        <li class="page-item">
                            <a href="#" class="page-link">1</a>
                        </li>
                        <li class="page-item active">
                            <a href="#" class="page-link">2</a>
                        </li>
                        <li class="page-item">
                            <a href="#" class="page-link" aria-label="Next">
                                <i class="ti-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </nav> --}}

                {{ $articles->links('vendor.pagination.bootstrap-4') }}

            @else
                <div class="d-block border text-center w-100 p-5">
                    <img src="{{ asset('actus/assets/img/sad-tear2.png') }}" alt="Oups!" width="100"> <br>
                    <small class="emty-text" style="font-style: italic;">Aucun article pour cette rubrique, arrive bientôt ...</small>
                </div>
            @endif


        </div>

    </div>
</div>

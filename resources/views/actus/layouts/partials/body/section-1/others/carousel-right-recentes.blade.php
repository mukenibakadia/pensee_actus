@php
    use App\Models\Article;
    $recent = Article::Published()->get()->sortByDesc('created_at')->take(5);
@endphp

@foreach ($recent as $article)
    <div class="trand-right-single d-flex">
        <div class="trand-right-img">
            @if (Storage::disk('public')->exists($article->image))
                <img src="{{ Voyager::image($article->thumbnail('cropped')) }}" alt="{{ $article->titre }}" style="width: 120px;height: 100px;object-fit: cover;">
            @else
                <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}" style="width: 120px;height: 100px;object-fit: cover;">
            @endif
        </div>
        <div class="trand-right-cap">
            <span class="color1 bg-color-1"><a href="{{ route('actus.articles.by.category', ['slug' => $article->categories->first()->slug]) }}">{{ $article->categories->first()->nom }}</a></span>
            <h4><a href="{{ route('actus.article.unique', ['category' => $article->categories->first()->slug, 'slug' => $article->slug]) }}">{{ $article->titre }}</a></h4>
        </div>
    </div>
@endforeach


@php
    use App\Models\Article;
    $inFront = Article::Published()->Infront()->get()->sortByDesc('created_at')->take(4);
@endphp

<div class="trending-bottom">
    <div class="row">
        @foreach ($inFront as $article)

            @if ($loop->first)
                <div class="trending-top mb-30 col-12">
                    <div class="trend-top-img">
                        @if (Storage::disk('public')->exists($article->image))
                            <img src="{{ Voyager::image($article->image) }}" alt="{{ $article->titre }}" style="width: 100%;height: 466px;object-fit: cover;">
                        @else
                            <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}" style="width: 100%;height: 466px;object-fit: cover;">
                        @endif
                        <div class="trend-top-cap">
                            <span class="bg-color-1"><a href="{{ route('actus.articles.by.category', ['slug' => $article->categories->first()->slug]) }}">{{ $article->categories->first()->nom }}</a></span>
                            <small class="d-inline ml-5 text-white">
                                <i class="fa fa-calendar-check"></i> Le {{ FrontEnd::dateEnFrancais($article->created_at) }}
                                &nbsp;|&nbsp;
                                <i class="fa fa-user"></i> {{ $article->auteurs->first()->nom }}
                                &nbsp;|&nbsp;
                                <i class="fa fa-eye"></i> {{ $article->clicks ?? 0 }} @if($article->clicks != null && $article->clicks > 0) vues @else vue @endif
                                &nbsp;|&nbsp;
                                <i class="fa fa-list-alt"></i> {{ FrontEnd::tempsLecture($article->contenu) }} de lecture
                            </small>
                            <h2><a href="{{ route('actus.article.unique', ['category' => $article->categories->first()->slug, 'slug' => $article->slug]) }}">{{ $article->titre }}</a></h2>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-4">
                    <div class="single-bottom mb-35">
                        <div class="trend-bottom-img mb-2" style="position: relative">
                            @if (Storage::disk('public')->exists($article->image))
                                <img src="{{ Voyager::image($article->image) }}" alt="{{ $article->titre }}" style="height: 180px;object-fit: cover;">
                            @else
                                <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}" style="height: 180px;object-fit: cover;">
                            @endif
                            <span class="color1 bg-color-1" style="position: absolute;bottom: 0;left:0px;padding: 10px 15px;font-size: 11px;font-weight: 400;line-height: 1;text-transform: uppercase;"><a href="{{ route('actus.articles.by.category', ['slug' => $article->categories->first()]) }}">{{ $article->categories->first()->nom }}</a></span>
                        </div>
                        <div class="trend-bottom-cap">
                            <p class="mb-0" style="font-size: 11px;">
                                Publié le {{ FrontEnd::dateEnFrancais($article->created_at) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </p>
                            <h4><a href="{{ route('actus.article.unique', ['category' => $article->categories->first()->slug, 'slug' => $article->slug]) }}">{{ $article->titre }}</a></h4>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>

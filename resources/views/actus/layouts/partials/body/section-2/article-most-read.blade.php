
@php
    use App\Http\Controllers\ArticlesController;
    $articles = ArticlesController::mostRead(5);
@endphp

<div class="weekly-news-area pt-50">
    <div class="container">
       <div class="weekly-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle mb-30">
                        <h3>Les plus lus</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="weekly-news-active dot-style d-flex dot-style">

                        @foreach ($articles as $article)

                            <div class="weekly-single">
                                <div class="weekly-img">
                                    @if (Storage::disk('public')->exists($article->image))
                                        <img src="{{ Voyager::image($article->thumbnail('medium')) }}" alt="{{ $article->titre }}">
                                    @else
                                        <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}">
                                    @endif

                                </div>
                                <div class="weekly-caption">
                                    <span class="color1 bg-color-1"><a href="{{ route('actus.articles.by.category', ['slug' => $article->categories->first()->slug]) }}">{{ $article->categories->first()->nom }}</a></span>
                                    <h4><a href="{{ route('actus.article.unique', ['category' => $article->categories->first()->slug, 'slug' => $article->slug]) }}">{{ $article->titre }}</a></h4>
                                </div>
                            </div>

                        @endforeach

                    </div>
                </div>
            </div>

       </div>
    </div>
</div>

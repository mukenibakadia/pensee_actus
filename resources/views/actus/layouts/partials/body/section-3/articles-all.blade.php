@php
    use App\Http\Controllers\ArticlesCategoriesController;
    $categories = ArticlesCategoriesController::categories();
    //dd($categories);
    $i = 0;
@endphp

@foreach ($categories as $key => $category)

    <div class="weekly2-news-area  weekly2-pading @if(($i%2) == 0) {{ 'gray-bg' }} @else {{ 'bg-white' }} @endif py-5">
        <div class="container">
            <div class="weekly2-wrapper">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle mb-30">
                            <h3>{{ $category->nom }}</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="row">

                            @foreach ($category->articles as $article)

                                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                    <div class="weekly2-single mx-0">
                                        <div class="weekly2-img">
                                            @if (Storage::disk('public')->exists($article->image))
                                                <img src="{{ Voyager::image($article->thumbnail('small')) }}" alt="{{ $article->titre }}">
                                            @else
                                                <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}">
                                            @endif
                                        </div>
                                        <div class="weekly2-caption">
                                            <span class="color1 bg-color-1"><a href="{{ route('actus.articles.by.category', ['slug' => $category->slug]) }}">{{ $category->nom }}</a></span>
                                            <p>Publié le {{ FrontEnd::dateEnFrancais($article->created_at) }}</p>
                                            <h4><a href="{{ route('actus.article.unique', ['category' => $category->slug, 'slug' => $article->slug]) }}">{{ $article->titre }}</a></h4>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @php
        $i++;
    @endphp

@endforeach

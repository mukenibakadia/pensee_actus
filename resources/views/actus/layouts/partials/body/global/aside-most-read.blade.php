@php
    use App\Http\Controllers\ArticlesController;
    $articles = ArticlesController::mostRead(5, $category_id);
@endphp

<aside class="single_sidebar_widget popular_post_widget border">

    <h3 class="widget_title">Le plus lus</h3>

    @foreach ($articles as $article)
        <div class="media post_item">
            @if (Storage::disk('public')->exists($article->image))
                <img src="{{  Voyager::image($article->thumbnail('cropped')) }}" alt="{{ $article->titre }}" style="width: 100px;object-fit: cover;">
            @else
                <img src="{{ asset('actus/assets/img/img-not-found.png') }}" alt="{{ $article->titre }}" style="width: 100px;object-fit: cover;">
            @endif
            <div class="media-body">
                <a href="{{ route('actus.article.unique', ['category' => $article->categories->first()->slug, 'slug' => $article->slug]) }}">
                    <h3>{{ $article->titre }}</h3>
                </a>
                <p>Publié le {{ FrontEnd::dateEnFrancais($article->created_at) }}</p>
            </div>
        </div>

        @if (!$loop->last)
            <hr>
        @endif

    @endforeach

</aside>

@php
    use App\Models\Article;
    $flashs = Article::FlasNews()->get();
    //dd($inFront);
@endphp
<!-- Trending Tittle -->
<div class="row">
    <div class="col-lg-12">
        <div class="trending-tittle">
            <strong>Flash news</strong>
            <!-- <p>Rem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
            <div class="trending-animated">
                <ul id="js-news" class="js-hidden">
                    @foreach ($flashs as $flash)
                        <li class="news-item">
                            <a href="{{ route('actus.article.unique', ['category' => $flash->categories->first()->slug, 'slug' => $flash->slug]) }}">
                                {{ $flash->titre }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>
</div>

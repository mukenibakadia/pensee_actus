<aside class="single_sidebar_widget newsletter_widget border">
    <h4 class="widget_title mb-3">Newsletter</h4>

    <p style="line-height: 1.2;">Rester informé en vous s'inscrivant à notre newsletter</p>

    <form action="#">
        <div class="form-group">
            <input type="email" class="form-control" onfocus="this.placeholder = ''"
                onblur="this.placeholder = 'Enter email'" placeholder='Votre adresse email' required>
        </div>
        <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
            type="submit">S'inscrire</button>
    </form>
</aside>

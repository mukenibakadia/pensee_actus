{{-- CAROUSEL --}}
<div class="trending-area fix">
    <div class="container">
        <div class="trending-main">
            @include('actus.layouts.partials.body.global.flash-news')
            @include('actus.layouts.partials.body.section-1.section1-carousel')
        </div>
    </div>
</div>
{{-- CAROUSEL --}}

{{-- LE PLUS LU --}}
@include('actus.layouts.partials.body.section-2.article-most-read')
{{-- LE PLUS LU --}}

{{-- ARTICLES RECENTS --}}
@include('actus.layouts.partials.body.section-3.articles-all')
{{-- ARTICLES RECENTS --}}



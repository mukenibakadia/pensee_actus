@php
    $category_id;
    if($articles->first())
    {
        $category_id = $articles->first()->categories->first()->id;
    }else{
        $category_id = 1;
    }
@endphp
<div class="container">
    <div class="row">
        <div class="col-12 my-4">
            @include('actus.layouts.breadcrumb')
        </div>
    </div>
</div>

<section class="blog_area section-padding py-0">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 mb-5 mb-lg-0">
                @include('actus.layouts.partials.body.section-1.sections1-article-category')
            </div>

            <div class="col-lg-4">

                <div class="blog_right_sidebar">

                    @include('actus.layouts.partials.body.global.aside-most-read', ['category_id' => $category_id])

                    @include('actus.layouts.partials.body.global.aside-newsletter')

                </div>

            </div>

        </div>
    </div>
</section>

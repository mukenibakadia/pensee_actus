<div class="container">
    <div class="row">
        <div class="col-12 my-4">
            @include('actus.layouts.breadcrumb')
        </div>
    </div>
</div>

<section class="contact-section py-0">
    <div class="container">

    <div class="row">
        <div class="col-12">

        </div>
        <div class="col-lg-8">
            <div class="d-block bg-white p-5 mb-4 border-top" style="box-shadow: 0px 10px 20px 0px rgba(221, 221, 221, 0.3);">
                <h2 class="contact-title" style="font-size: 20px;margin-bottom: 40px;">{{ $info->titre }}</h2>

                <div class="contact-content">
                    {!! $info->contenu !!}
                </div>

                <h2 class="contact-title mt-5" style="font-size: 20px;margin-bottom: 40px;">Nous écrire</h2>
                <form class="form-contact contact_form" action="" method="post" id="contactForm" novalidate="novalidate">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Sujet'" placeholder="Sujet">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Noms'" placeholder="Noms">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Adresse email'" placeholder="Adresse email">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'" placeholder=" Message"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="button button-contactForm boxed-btn">Envoyer</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-4">

            <div class="d-block" style="background: #fbf9ff;padding: 30px;margin-bottom: 30px;">

                <h3 style="font-size: 20px;margin-bottom: 40px;border-bottom: 1px solid #f0e9ff;padding-bottom: 15px;">Nos contacts</h3>

                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                    <div class="media-body">
                        <h3>+243 825 757 570</h3>
                        <h3>+243 894 707 943</h3>
                        <p>Joignable 24/24</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-email"></i></span>
                    <div class="media-body">
                        <h3>contact@impartialinfo.net</h3>
                        <p>Envoyez nous un mail!</p>
                    </div>
                </div>
            </div>

            <div class="blog_right_sidebar">
                @include('actus.layouts.partials.body.global.aside-newsletter')
            </div>

        </div>
    </div>
</div>

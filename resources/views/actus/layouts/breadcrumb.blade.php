<nav aria-label="breadcrumb" class="d-none d-sm-block">

    <ol class="breadcrumb hidden-xs mb-0">

        @php
            $url = route('actus.home');
            $segments = array_filter(explode('/', substr(Request::url(), strlen($url), strlen(Request::url()))));
        @endphp

        @if(count($segments) == 0)
            <li class="breadcrumb-item active">Accueil</li>
        @else

            <li class="breadcrumb-item active">
                <a href="{{ $url }}"><i class="fa fa-home"></i> Accueil</a>
            </li>

            @foreach ($segments as $segment)

                @php
                    $url .= '/'.$segment;
                @endphp

                @if($segment == 'articles')
                    @php
                        continue;
                    @endphp
                @else
                    @if ($loop->last)
                        <li class="breadcrumb-item">{{ ucfirst(str_replace('-',' ',urldecode($segment))) }}</li>
                    @else
                        <li class="breadcrumb-item">
                            <a href="{{ $url }}">{{ ucfirst(str_replace('-',' ',urldecode($segment))) }}</a>
                        </li>
                    @endif
                @endif

            @endforeach

        @endif

    </ol>

</nav>

@extends('actus.layouts.master')

@section('meta')
    <title>{{ setting('site.title') }} | {{ __('Nous contacter') }}</title>
@endsection

@section('body')

    @include('actus.layouts.partials.body.body-contact')

@endsection

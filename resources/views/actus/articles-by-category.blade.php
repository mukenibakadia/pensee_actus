@extends('actus.layouts.master')

@section('meta')
    <title>{{ setting('site.title') }} | {{ __('Article') }} - {{ __('Catégorie') }} {{ $categoryName }}</title>
@endsection

@section('body')

    @include('actus.layouts.partials.body.body-articles-by-category')

@endsection

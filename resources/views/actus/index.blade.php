@extends('actus.layouts.master')

@section('meta')
    <title>{{ setting('site.title') }} | {{ __('Page d\'accueil') }}</title>
@endsection

@section('body')

    @include('actus.layouts.partials.body.body-index')

@endsection
